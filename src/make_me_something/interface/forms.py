from django import forms
from django.contrib.auth.models import User
from bootstrap_toolkit.widgets import BootstrapDateInput, BootstrapTextInput, BootstrapUneditableInput
from captcha.fields import CaptchaField

class FormSetInlineForm(forms.Form):
    UserName = forms.CharField()
    Password = forms.CharField(widget = forms.PasswordInput)

class RegistrationForm(forms.Form):
  name = forms.CharField()
  username = forms.CharField()
  email = forms.EmailField()
  password = forms.CharField(widget=forms.PasswordInput) # Set the widget to
                                                         # PasswordInput
  password2 = forms.CharField(widget=forms.PasswordInput,
                              label="Confirm password") # Set the widget to
                                                        # PasswordInput and
                                                        # set an appropriate
                   
                                                       # label
def clean_password2(self):
    password = self.cleaned_data['password'] # cleaned_data dictionary has the
                                             # the valid fields
    password2 = self.cleaned_data['password2']
    if password != password2:
      raise forms.ValidationError("Passwords do not match.")
    return password2

class PostForm(forms.Form):
    title = forms.CharField(max_length=250,)
    content = forms.CharField(max_length=5000,)


class ReplyForm(forms.Form):
    content = forms.CharField(max_length=5000,)


    

