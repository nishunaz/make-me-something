# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models


class User(models.Model):
    user_id = models.IntegerField(primary_key=True)
    user_name = models.CharField(max_length=135, blank=True)
    password = models.CharField(max_length=135, blank=True)
    email = models.CharField(max_length=135,blank=True)
    name = models.CharField(max_length=135,blank=True)
    status = models.BooleanField()
    banned = models.BooleanField()
    class Meta:
        db_table = u'user'

class Forum_Post(models.Model):
    post_id = models.AutoField(primary_key=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    user_id = models.ForeignKey(User)
    popularity = models.IntegerField()
    title = models.TextField(blank=True)
    content = models.TextField(blank=True)
    subject = models.CharField(max_length=135,blank=True)
    replies = models.IntegerField()
    class Meta:
		db_table = u'post'
		
class Reply(models.Model):
    reply_id = models.AutoField(primary_key=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    post_id = models.ForeignKey(Forum_Post)
    user_id = models.ForeignKey(User)
    expert_approvals = models.IntegerField()
    approvals = models.IntegerField()
    disapprovals = models.IntegerField()
    verifications = models.IntegerField()
    content = models.TextField(blank=True)
    class Meta:
		db_table = u'reply'

class User_Vote(models.Model):
    user_id = models.ForeignKey(User)
    reply_id = models.ForeignKey(Reply)
    expert_approvals = models.BooleanField()
    approvals = models.BooleanField()
    disapprovals = models.BooleanField()
    verifications = models.BooleanField()
    class Meta:
        db_table = u'user_vote'

class User_Post_Vote(models.Model):
    user_id = models.ForeignKey(User)
    post_id = models.ForeignKey(Forum_Post)
    voted = models.BooleanField()
    class Meta:
        db_table = u'user_post_vote'
