from django.shortcuts import render
from import_elements import *
from .forms import RegistrationForm, PostForm, ReplyForm
from .models import *

# Login Page:

def login_page(request):
    c = {}
    c.update(csrf(request))
    if request.method == 'GET':
        request.session['userid'] = None
        theTime = time.localtime()
        c['formset'] = formset_factory(FormSetInlineForm)
        return render_to_response('login.html', c)

    elif request.method == 'POST':
        username = request.POST['form-0-UserName']
        password = request.POST['form-0-Password']
        user = User.objects.filter( user_name = username)
        if len(user) != 0:
            user = user[0]
            if password == user.password:
                request.session['userid'] = user.user_id
                if(User.objects.get(user_id=request.session['userid']).banned == 1):
                    return HttpResponse('You have been banned')
                return HttpResponseRedirect('mainpage')
        c['formset'] = formset_factory(FormSetInlineForm)
        return render_to_response('login_failed.html', c)

    return HttpResponse('Invalid Request')


def registration_form(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
			newUser = User(user_id = len(User.objects.all()) + 1, user_name = request.POST['username'], password = request.POST['password'],email = request.POST['email'], name = request.POST['name'],status = 0, banned = 0)
			newUser.save()
			return render(request, "success.html")
    else:
        form = RegistrationForm()
        return render(request, "registration_form.html",{ "form" : form })

def mainpage(request):
    if request.session.get('userid') == None :
		return HttpResponse('Access Denied. You must be logged in to view this page.')
    top_posts_craft = Forum_Post.objects.filter(subject = 'craft').order_by('-popularity')[:5]
    top_posts_electronics = Forum_Post.objects.filter(subject = 'electronics').order_by('-popularity')[:5]
    top_posts_food = Forum_Post.objects.filter(subject = 'food').order_by('-popularity')[:5]
    return render_to_response('mainpage.html', RequestContext(request, {
        'crafts': top_posts_craft,
        'electronics': top_posts_electronics ,
        'food': top_posts_food,
    }))

def craft(request):
    if request.session.get('userid') == None :
        return HttpResponse('Access Denied. You must be logged in to view this page.')
    craft_posts_recent = Forum_Post.objects.filter(subject = 'craft').order_by('-timestamp')
    craft_posts_top = Forum_Post.objects.filter(subject='craft').order_by('-popularity')
    return render_to_response('craft.html', RequestContext(request, {
        'recent': craft_posts_recent,
        'top': craft_posts_top ,
    }))


def electronics(request):
	if request.session.get('userid') == None :
		return HttpResponse('Access Denied. You must be logged in to view this page.')
	electronics_posts_recent = Forum_Post.objects.filter(subject = 'electronics').order_by('-timestamp')
	electronics_posts_top = Forum_Post.objects.filter(subject='electronics').order_by('-popularity')
	return render_to_response('electronics.html', RequestContext(request, {
		'recent': electronics_posts_recent,
		'top': electronics_posts_top ,
    }))


def food(request):
	if request.session.get('userid') == None :
		return HttpResponse('Access Denied. You must be logged in to view this page.')
	food_posts_recent = Forum_Post.objects.filter(subject = 'food').order_by('-timestamp')
	food_posts_top = Forum_Post.objects.filter(subject='food').order_by('-popularity')
	return render_to_response('food.html', RequestContext(request, {
		'recent': food_posts_recent,
		'top': food_posts_top ,
    }))

def display_post(request, id = None, *args, **kwargs):
    if request.session.get('userid') == None :
        return HttpResponse('Access Denied. You must be logged in to view this page.')
    if request.method == 'POST':
        if(request.POST.get('Delete')):
            Forum_Post.objects.get(post_id=id).delete()
            return HttpResponse('This post has been deleted')
        if(request.POST.get('Popularity')):
            post = Forum_Post.objects.filter(post_id = id)[0]
            post.popularity = post.popularity + 1
            post.save()
	    #return HttpResponse(str(post.popularity))
    post = Forum_Post.objects.get(post_id=id)
    username = User.objects.get(user_id=post.user_id.user_id).user_name
    request.session['userobj'] = User.objects.get(user_id=post.user_id.user_id)
    replies = Reply.objects.filter(post_id = post)
    user = User.objects.get(user_id=request.session['userid'])
    username2 = user.user_name
    # Generate CRSF token
    csrf_token = csrf(request)
    c_key = csrf_token.keys()[0]

    return render_to_response('display_post.html', RequestContext(request, {
    'replies': replies,
    'post': post,
    'username': username,
    'id': id,
    'username2':username2,
    c_key: csrf_token[c_key],
    }))

def reply_operation(request, action, reply_id):
    user = request.session['userobj']
    # Check if this is post upvote
    if action == 'upvote':
        thePost = Forum_Post.objects.get(post_id = reply_id)
        vote_record = User_Post_Vote.objects.filter(user_id = user, post_id = thePost)
	if len(vote_record) == 0:
	    vote_record = User_Post_Vote(user_id = user, post_id = thePost, voted = False)
	else:
	    vote_record = vote_record[0]
	if not vote_record.voted:
	    thePost.popularity = thePost.popularity + 1
	    vote_record.voted = True
	else:
	    thePost.popularity = thePost.popularity - 1
	    vote_record.voted = False
	vote_record.save()
	thePost.save()
	return HttpResponse(str(thePost.popularity))


    theReply = Reply.objects.get(reply_id = reply_id)
    # Load previous voting record for this post
    vote_record = User_Vote.objects.filter(user_id = user, reply_id = theReply)
    if len(vote_record) == 0:
        vote_record = User_Vote( user_id = user, reply_id = theReply, expert_approvals = False, approvals = False, disapprovals = False, verifications = False)
    else:
        vote_record = vote_record[0]

    if (action == 'verify'):
    	if not vote_record.verifications:
            toReturn = theReply.verifications = theReply.verifications + 1
	    vote_record.verifications = True
	else:
            toReturn = theReply.verifications = theReply.verifications - 1
	    vote_record.verifications = False

    elif action == 'approve':
        if not vote_record.approvals:
            toReturn = theReply.approvals = theReply.approvals + 1
	    vote_record.approvals = True
	else:
            toReturn = theReply.approvals = theReply.approvals - 1
	    vote_record.approvals = False
    elif action == 'disapprove':
        if not vote_record.disapprovals:
            toReturn = theReply.disapprovals = theReply.disapprovals + 1
	    vote_record.disapprovals = True
	else:
            toReturn = theReply.disapprovals = theReply.disapprovals - 1
	    vote_record.disapprovals = False
    elif action == 'expert_approve':
        if not vote_record.expert_approvals:
            toReturn = theReply.expert_approvals = theReply.expert_approvals + 1
	    vote_record.expert_approvals = True
	else:
            toReturn = theReply.expert_approvals = theReply.expert_approvals - 1
	    vote_record.expert_approvals = False

    theReply.save()
    vote_record.save()
    return HttpResponse(str(toReturn))

def post(request,sub = None, *args, **kwargs):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            user = User.objects.get(user_id=request.session['userid']);
            newPost = Forum_Post(user_id=user,popularity=0,title=request.POST['title'],content=request.POST['content'],subject= sub,replies=0)
            newPost.save()
            return HttpResponse('Post Added.\nYou may close this window now.')
    else:
        form = PostForm()
        return render(request,"new_post.html",{"form" :form})

def reply(request, postid = None, *args, **kwargs):
    if(request.method == 'POST'):
        form = ReplyForm(request.POST)
        if form.is_valid():
            print postid
            current_post = Forum_Post.objects.get(post_id = postid)
            current_post.replies = current_post.replies + 1;
            current_post.save()
            user = User.objects.get(user_id=request.session['userid'])
            newReply = Reply(post_id = current_post, user_id=user,expert_approvals=0,approvals=0,disapprovals=0,verifications=0,content=request.POST['content'])
            newReply.save()
            return HttpResponse('Reply Added.\nYou may close this window now.')
    else:
        form = ReplyForm()
        return render(request,"reply.html",{"form" :form})

def view_user(request, username=None, *args, **kwargs):
    user=User.objects.get(user_name=username)
    print user.user_id
    user.banned=1
    user.save()
    return HttpResponse('The user was successfully banned')
