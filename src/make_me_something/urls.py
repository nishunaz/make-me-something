from django.conf.urls import patterns,include, url
from registration.backends.default.views import RegistrationView



# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.views.generic import TemplateView

urlpatterns = patterns('',
	url(r'^$', 'interface.views.login_page',{}, name="home"),

	url(r'^static$', 'django.views.static.serve'),
	url(r'^test$', 'interface.alt.alt.test'),
    url(r'^registration_form', 'interface.views.registration_form'),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^mainpage', 'interface.views.mainpage'),
    url(r'^craft', 'interface.views.craft'),
    url(r'^food', 'interface.views.food'),
    url(r'^electronics', 'interface.views.electronics'),
    url(r'^display_post/(?P<id>\d+)/$','interface.views.display_post'),
    url(r'^newpost/(?P<sub>.+)','interface.views.post'),
    url(r'^reply/(?P<postid>\d+)/$','interface.views.reply'),
    url(r'^view_user/(?P<username>.+)','interface.views.view_user'),
    url(r'^reply_op/(?P<action>.+)/(?P<reply_id>\d+)/$', 'interface.views.reply_operation')
)
